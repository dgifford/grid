<?php
Namespace dgifford\Grid;

Use dgifford\Grid\Grid;




/**
 * Auto Loader
 * 
 */
require_once __DIR__ . '/../vendor/autoload.php';



class GridTest extends \PHPUnit\Framework\TestCase
{
	public $fill_array = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', ];

	public $default_grid = [ 
		['a', 'b', 'c', 'd', ],
		['e', 'f', 'g', 'h', ],
		['i', 'j', 'k', 'l', ],
		['m', 'n', 'o', 'p', ],
	];



	public function testEmptyConstructor()
	{
		$grid = new Grid;

		$this->expectException( '\InvalidArgumentException' );

		// Grid has zero size so no coords valid
		$grid->get( 0, 0 );
	}



	public function testGridSetInConstructor()
	{
		$grid = new Grid( $this->default_grid );

		$this->assertSame( 'a', $grid->get( 0, 0 ) );
	}



	public function testClearCoordinate()
	{
		$grid = new Grid( $this->default_grid );

		$this->assertSame( 'a', $grid->get( 0, 0 ) );

		$grid->clear( 0, 0 );

		$this->assertNull( $grid->get( 0, 0 ) );

		$this->assertSame( 'b', $grid->get( 1, 0 ) );
	}



	public function testClearGrid()
	{
		$grid = new Grid( $this->default_grid );

		$this->assertSame( 'a', $grid->get( 0, 0 ) );

		$grid->clear();

		$this->assertNull( $grid->get( 0, 0 ) );

		$this->assertNull( $grid->get( 1, 0 ) );
	}



	public function testGridSizeSetInConstructor()
	{
		$grid = new Grid( 3, 3 );

		$this->assertNull( $grid->get( 0, 0 ) );

		$this->assertSame( 3, $grid->x_max );
	}



	public function testGridSizeSetAndFilledInConstructor()
	{
		$grid = new Grid( 3, 3, $this->fill_array );

		$this->assertSame( 'a', $grid->get( 0, 0 ) );

		$this->assertSame( 'd', $grid->get( 0, 1 ) );
	}



	public function testSetGridWithMultiDimensionalArray()
	{
		$grid = new Grid;

		$grid->setGrid( $this->default_grid );

		$this->assertSame( 'a', $grid->get( 0, 0 ) );

		$this->assertSame( 4, $grid->x_max );

		$this->assertSame( 4, $grid->y_max );
	}



	public function testFillWithNormalArray()
	{
		$grid = new Grid;

		$grid->setSize( 4, 4 );

		$grid->fill( $this->fill_array );

		$this->assertSame( 'a', $grid->get( 0, 0 ) );

		$this->assertSame( 'e', $grid->get( 0, 1 ) );

		$this->assertNull( $grid->get( 3, 3 ) );

		$this->assertSame( 4, $grid->x_max );

		$this->assertSame( 4, $grid->y_max );
	}



	public function testGetUnfilledCoordinateIsNull()
	{
		$grid = new Grid;

		$grid->setSize( 4, 4 );

		$this->assertNull( $grid->get( 0, 0 ) );

		$grid->fill( $this->fill_array );

		$this->assertSame( 'a', $grid->get( 0, 0 ) );
	}



	public function testValidCoordinates()
	{
		$grid = new Grid( 4, 4 );

		$this->assertTrue( $grid->validCoordinates( 0, 0 ) );

		$this->assertTrue( $grid->validCoordinates( 3, 3 ) );

		$this->assertFalse( $grid->validCoordinates( 0, 4 ) );

		$this->assertFalse( $grid->validCoordinates( 4, 3 ) );

		$this->assertFalse( $grid->validXY( false, false ) );
	}



	public function testGetInvalidCoordinateException()
	{
		$grid = new Grid;

		$grid->setSize( 4, 4 );

		$this->expectException('\InvalidArgumentException');

		$grid->get( 5, 5 );

		$this->assertNull( $grid->get( 0, 0 ) );
	}



	public function testgetValidAdjacentCoordinates()
	{
		$grid = new Grid;

		// 4x4 grid
		$grid->setGrid( $this->default_grid );

		$this->assertSame( [[1,1], [1,0], [0,1] ], $grid->getValidAdjacentCoordinates(0,0) );

		$this->assertSame( 5, count($grid->getValidAdjacentCoordinates(1,0)) );

		$this->assertSame( [[2,1], [2,0], [0,0], [0,1], [1,1] ], $grid->getValidAdjacentCoordinates(1,0) );
	}



	public function testFlatten()
	{
		$grid = new Grid;

		// 4x4 grid
		$grid->setGrid( $this->default_grid );

		$this->assertSame( [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', ], $grid->flatten() );

		// Set one position as an array
		$grid->set( 0,0, ['a','a','a',]);

		$this->assertSame( [ ['a','a','a',], 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', ], $grid->flatten() );
	}



	public function testShuffle()
	{
		$grid = new Grid;

		// 4x4 grid
		$grid->setGrid( $this->default_grid );

		$this->assertSame( [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', ], $grid->flatten() );

		$grid->shuffle();

		$this->assertFalse( ([ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', ] == $grid->flatten()) );

	}



	public function testEach()
	{
		$grid = new Grid;

		// 4x4 grid
		$grid->setGrid( $this->default_grid );

		$this->assertSame( [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', ], $grid->flatten() );

		// Set one position as an array
		$grid->set( 0,0, ['a','a','a',]);

		$grid->each( function($value){ return ((is_string($value)) ? strtoupper($value) : $value); } );

		$this->assertSame( [ ['a','a','a',], 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', ], $grid->flatten() );

	}



	public function testStringToCoordinates()
	{
		$grid = new Grid;

		// 4x4 grid
		$grid->setGrid( $this->default_grid );

		$this->assertSame( [0, 0], $grid->stringToXY( '0,0' ) );
	}



	public function testCoordinatesAsString()
	{
		$grid = new Grid;

		// 4x4 grid
		$grid->setGrid( $this->default_grid );

		$this->assertSame( '0,0', $grid->XYToString( 0, 0 ) );

		$this->assertSame( '0,0', $grid->XYToString( [0, 0] ) );
	}



	public function testNthToXY()
	{
		$grid = new Grid;

		// 4x4 grid
		$grid->setGrid( $this->default_grid );

		$this->assertSame( [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', ], $grid->flatten() );

		$this->assertSame( [ 0,0 ], $grid->nthToXY( 0, false ) );

		$this->assertSame( [ 0,1 ], $grid->nthToXY( 4, false ) );
	}



	public function testInvalidCoordinatesAsString()
	{
		$grid = new Grid;

		// 4x4 grid
		$grid->setGrid( $this->default_grid );

		$this->expectException( '\InvalidArgumentException' );

		$grid->XYToString( 0, 5 );

		$this->expectException( '\InvalidArgumentException' );

		$grid->XYToString( [0, 0], 0 );
	}



	public function testFind()
	{
		$grid = new Grid;

		$grid->setGrid( $this->default_grid );

		$this->assertSame( [[0,0]], $grid->find('a') );

		$grid->set( 1,2, 'a' );

		$this->assertSame( [[0,0], [1,2]], $grid->find('a') );

	}



	public function testDifference()
	{
		$grid = new Grid;

		$grid->setGrid( $this->default_grid );

		$this->assertSame( [1,1], $grid->difference( 1, 1, 2, 2 ) );

		$this->assertSame( [2,2], $grid->difference( 0, 0, 2, 2 ) );

		$this->assertSame( [-1,0], $grid->difference( 1, 1, 0, 1 ) );
	}



	public function testContains()
	{
		$grid = new Grid;

		$grid->setGrid( $this->default_grid );

		$this->assertFalse( $grid->contains( 'z' ) );

		$this->assertTrue( $grid->contains( 'a' ) );
	}





}