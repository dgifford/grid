<?php
Namespace dgifford\Grid;


/*
	Grid class allows accessing values in a two dimensional grid using
	coordinates.

	The grid is 0 indexed. For example, in a 4x4 grid coordinate 0,0 is
	the left bottom corner and 3,3 is the top right.


    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\Traits\ArrayHelpersTrait;



class Grid
{
	Use ArrayHelpersTrait;

	public $grid = [];

	public $x;

	public $y;

	public $x_max = 0;

	public $y_max = 0;

	protected $xy_as_string = false;






	/**
	 * Create grid object.
	 *
	 * Accepts a single argument, a multi dimensional array
	 * which is used to create the grid.
	 *
	 * or
	 *
	 * 2 integers ( x, y ) defining the size of the grid plus a 
	 * 3rd optional array used to fill the grid.
	 * 
	 */
	function __construct()
	{
		$args = func_get_args();

		// Single multidimensional array provided
		if( count( $args ) == 1 and is_array($args[0]) )
		{
			$this->setGrid( $args[0] );
		}
		// Grid size provided
		elseif( count( $args ) > 1 and is_int($args[0]) and is_int($args[1]) )
		{
			$this->setXMax( $args[0] );

			$this->setYMax( $args[1] );

			// Fill using array
			if( isset($args[2]) and is_array($args[2]) )
			{
				$this->fill($args[2]);
			}
		}
	}



	/**
	 * Set all coordinates to be in string format 'x,y'
	 */
	public function setXYAsString( $bool = true )
	{
		if( is_bool($bool) )
		{
			$this->xy_as_string = $bool;
		}
		else
		{
			$this->xy_as_string = true;
		}
	}



	/**
	 * Set the grid using a multi-dimensional array.
	 *
	 * Optionally calculate the size of the grid from the
	 * number of rows and the length of the longest row.
	 * 
	 * @param array   $grid 
	 * @param boolean $calculate_size
	 */
	public function setGrid( $grid = [], $calculate_size = true )
	{
		// Simple check for multidimensional array
		if( $this->isMultidimensional($grid) )
		{
			$this->grid = $grid;

			if( $calculate_size )
			{
				$this->setYMax( count($this->grid) );

				foreach( $this->grid as $row )
				{
					if( is_array($row) and count($row) > $this->x_max )
					{
						$this->setXMax( count($row) );
					}
				}
			}
		}
	}



	/**
	 * Randomize the order of the grid.
	 * 
	 * @return null
	 */
	public function shuffle()
	{
		foreach( $this->grid as &$row )
		{
			shuffle( $row );
		}

		shuffle( $this->grid );
	}



	/**
	 * Return a count of the number of positions in the
	 * grid.
	 * 
	 * @return integer
	 */
	public function count()
	{
		return $this->x_max * $this->y_max;
	}



	/**
	 * Return the grid with all rows flattened to
	 * a single array.
	 * 
	 * @return array
	 */
	public function flatten()
	{
		$result = [];

		foreach( $this->grid as &$row )
		{
			foreach( $row as &$value )
			{
				$result[] = $value;
			}
		}

		return $result;
	}



	/**
	 * Fill the grid from 0,0 row by row using the contents of an
	 * array.
	 * 
	 * @param  array  $arr
	 * @return null
	 */
	public function fill( $arr = [] )
	{
		if( !$this->valid() )
		{
			throw new \InvalidArgumentException('The grid size is invalid.');
		}

		if( is_array( $arr ) )
		{
			foreach( $arr as $key => $value )
			{
				$xy = $this->nthToXY( $key );

				$this->set( $xy[0], $xy[1], $value );
			}
		}
	}



	/**
	 * Perform an operation on each item in the grid.
	 * The callable is provided with the $value, followed by
	 * the x and y coordinates
	 * 
	 * @param  callable $callable
	 * @return null
	 */
	public function each( callable $callable )
	{
		foreach( $this->grid as $y => $row )
		{
			foreach( $row as $x => $value )
			{
				$this->grid[$y][$x] = call_user_func_array( $callable, [ $value, $x, $y, ] );
			}
		}
	}



	/**
	 * If coordinates supplied, set the value to null.
	 *
	 * If no arguments provided, clear whole grid.
	 * 
	 * @return null
	 */
	public function clear()
	{
		$args = func_get_args();

		if( count($args) == 2 )
		{
			$this->set( $args[0], $args[1] );
		}
		elseif( count($args) == 0 )
		{
			$this->grid = [];
		}
	}



	/**
	 * Set the value at a coordinate.
	 *
	 * Throws an exception if the coordinates are invalid.
	 * 
	 * @param int $x
	 * @param int $y
	 * @param mixed $value
	 */
	public function set( $x, $y, $value = null )
	{
		if( $this->validCoordinates( $x, $y ) )
		{
			if( !isset( $this->grid[$y] ) )
			{
				$this->grid[$y] = [];
			}

			$this->grid[$y][$x] = $value;
		}
		else
		{
			throw new \InvalidArgumentException('Invalid grid coordinates.');
		}
	}



	/**
	 * Returns the value at a coordinate.
	 * 
	 * @param int $x
	 * @param int $y
	 * @return mixed
	 */
	public function get( $x, $y )
	{
		if( !$this->validCoordinates( $x, $y ) )
		{
			throw new \InvalidArgumentException('Invalid grid coordinates.');
		}

		if( !isset($this->grid[$y]) or !isset($this->grid[$y][$x]) )
		{
			return null;
		}
		else
		{
			return $this->grid[$y][$x];
		}
	}



	/**
	 * Returns an array of coordinates adjacent to those provided.
	 * 
	 * @param int $x
	 * @param int $y
	 * @return array    Array of arrays in format [x , y]
	 */
	public function getValidAdjacentCoordinates( $x, $y )
	{
		$result = [];

		$adjacent_coords = 
		[
			[1,1],
			[1,0],
			[1,-1],
			[0,-1],
			[-1,-1],
			[-1,0],
			[-1,1],
			[0,1],
		];

		foreach( $adjacent_coords as $xy )
		{
			if( $this->validCoordinates( $x + $xy[0], $y + $xy[1] ) )
			{
				$result[] = [$x + $xy[0], $y + $xy[1]];
			}
		}

		return $result;
	}



	/**
	 * Returns the value at a relative coordinate.
	 *
	 * Throws exceptions if the orgin coordinates or the 
	 * relative values are invalid
	 * 
	 * @param  integer $x
	 * @param  integer $y
	 * @param  integer $x_rel  Amount to change x by
	 * @param  integer $y_rel  Amount to change y by
	 * @return mixed           Value at relative coordinate
	 */
	public function getRelative( $x, $y, $x_rel = 0, $y_rel = 0 )
	{
		if( !$this->validCoordinates( $x, $y ) )
		{
			throw new \InvalidArgumentException('Invalid grid coordinates.');
		}

		if( !is_int( $x_rel ) or !is_int( $y_rel ) )
		{
			throw new \InvalidArgumentException('Relative coordinates must be integers.');
		}

		return $this->get( $x + $x_rel, $y + $y_rel );
	}



	/**
	 * Calculate the relative difference between 2 coordinates.
	 * 
	 * @param  integer $x 
	 * @param  integer $y
	 * @param  integer $x2
	 * @param  integer $y2 
	 * @return array
	 */
	public function difference( $x, $y, $x2, $y2 )
	{
		if( !$this->validCoordinates( $x, $y ) or !$this->validCoordinates( $x2, $y2 ) )
		{
			throw new \InvalidArgumentException('Invalid grid coordinates.');
		}

		return [ ($x2 - $x), ($y2 - $y)];
	}



	/**
	 * Boolean test that coordinates are within the boundaries
	 * of the grid.
	 * 
	 * @param  integer $x
	 * @param  integer $y
	 * @return boolean
	 */
	public function validCoordinates( $x, $y )
	{
		if( is_int($x) and is_int($y) and $x < $this->x_max and $x >= 0 and $y < $this->y_max and $y >= 0 )
		{
			return true;
		}

		return false;
	}



	/**
	 * Alias of validCoordinates()
	 */
	public function validXY( $x, $y )
	{
		return call_user_func_array([$this, 'validCoordinates' ], func_get_args() );
	}



	/**
	 * Boolean test for a valid grid, where max dimensions
	 * are positive integers.
	 * 
	 * @return boolean
	 */
	public function valid()
	{
		if( $this->x_max < 1 or $this->y_max < 1 or !is_int($this->x_max) or !is_int($this->y_max) )
		{
			return false;
		}

		return true;;
	}



	/**
	 * Returns the coordinates of the nth item counting
	 * along each row from 0,0, row by row.
	 * 
	 * @param  integer $index
	 * @return array         x, y
	 */
	public function nthToXY( $index )
	{
		$result = [];

		if( $index > 0 )
		{
			// X coordinate
			$x = $index % $this->x_max;

			// y coordinate
			$y = intval( floor( $index / $this->y_max ) );
		}
		else
		{
			$x = 0;
			$y = 0;
		}

		$result = [ $x, $y ];

		return $result;
	}



	/**
	 * Convert a string in the format 'x,y' into an array
	 * of coordinates
	 * 
	 * @param  string $xy
	 * @return array
	 */
	public function stringToXY( $xy )
	{
		if( is_string($xy) )
		{
			$xy = explode(',', $xy);

			if( count($xy) == 2 )
			{
				$x = intval( trim($xy[0]) );
				$y = intval( trim($xy[1]) );

				if( !$this->validCoordinates( $x, $y ) )
				{
					throw new \InvalidArgumentException('The coordinates are invalid.');
				}
				else
				{
					return [$x, $y];
				}
			}
		}

		throw new \InvalidArgumentException('The string coordinates are invalid.');
	}



	/**
	 * Return the coordinate(s) that contain the value.
	 * 
	 * @param  mixed $value  Value to find
	 * @return array
	 */
	public function find( $needle )
	{
		$result = [];

		for( $y=0; $y < $this->y_max; $y++ )
		{ 
			for( $x=0; $x < $this->x_max; $x++ )
			{
				if( $this->grid[$y][$x] === $needle )
				{
					$result[] = [ $x, $y ];
				}
			}
		}

		return $result;
	}



	/**
	 * Boolean test if the grid contains a value.
	 * 
	 * @param  mixed $needle
	 * @return boolean
	 */
	public function contains( $needle )
	{
		for( $y=0; $y < $this->y_max; $y++ )
		{ 
			for( $x=0; $x < $this->x_max; $x++ )
			{
				if( $this->grid[$y][$x] === $needle )
				{
					return true;
				}
			}
		}

		return false;
	}



	/**
	 * Convert coordinates into a string format 'x,y'.
	 * Accepts a single coordinate array arguments or two
	 * arguments x,y
	 * 
	 * @param mixed $x   Integer or array of coordinates.
	 * @param [type] $y [description]
	 */
	public function XYToString( $x, $y = null )
	{
		if( is_array($x) and count($x) == 2 and is_null($y) and is_int($x[0]) and is_int($x[1]))
		{
			$y = $x[1];
			$x = $x[0];
		}

		if( !isset($x) or !isset($y) or !$this->validCoordinates( $x, $y ) )
		{
			throw new \InvalidArgumentException('The coordinates are invalid.');
		}
		else
		{
			return "{$x},{$y}";
		}
	}



	/**
	 * Set the max dimensions of the grid.
	 *
	 * Throws an exception if the values are invalid.
	 * 
	 * @param integer $x_max
	 * @param integer $y_max
	 */
	public function setSize( $x_max, $y_max )
	{
		$this->setXMax( $x_max );

		$this->setYMax( $y_max );

		if( !$this->valid() )
		{
			throw new \InvalidArgumentException('The grid size is invalid.');
		}
	}



	/**
	 * Set the max x dimension of the grid.
	 * @param integer $x_max
	 */
	public function setXMax( $x_max = 0 )
	{
		if( is_int($x_max) and $x_max > 0 )
		{
			$this->x_max =$x_max;
		}
	}



	/**
	 * Set the max y dimension of the grid.
	 * @param integer $y_max
	 */
	public function setYMax( $y_max = 0 )
	{
		if( is_int($y_max) and $y_max > 0 )
		{
			$this->y_max = $y_max;
		}
	}
}